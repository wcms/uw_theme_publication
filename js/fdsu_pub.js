/**
 * @file
 */

(function ($, window, document) {

  var $window = $(window);
  var footerHeight = 240;
  var headerWrapper = $('.uwm-header-wrapper');
  var $parallaxId = '.uwm-article-cover-parallax'; // Class for setting hero image height of view.

  function is_ie() {
    // Disable for IE.
    var ua = window.navigator.userAgent;
    var old_ie = ua.indexOf('MSIE ');
    var new_ie = ua.indexOf('Trident/');

    if ((old_ie > -1) || (new_ie > -1)) {
      return true;
    }
  }

  function flipHeaderClass() {
    var windowSize = $(window).width();
    var logoHeight = $('#uwm-logo-header');
    var num = logoHeight.outerHeight(true);
    var headerWrapper = $('.uwm-header-wrapper');
    var menuOpen = $('.uwm-header-bar');
      if (windowSize < 1012) {
        if (document.body.scrollTop > num || document.documentElement.scrollTop > num) {
          $(logoHeight).addClass('hide-logo');
          $('.uwm-header-bar-wrapper').addClass('fixed-to-top');

          $(headerWrapper).css('margin-bottom', 172);

          if (menuOpen.hasClass('bt-menu-open')) {
            $(headerWrapper).css('margin-bottom', 0);
          }

        }
        else {
          $(logoHeight).removeClass('hide-logo');
          $('.uwm-header-bar-wrapper').removeClass('fixed-to-top');
          $(headerWrapper).css('margin-bottom', 0);
        }
      }
      else if (menuOpen.hasClass('bt-menu-open')) {
        $(headerWrapper).css('margin-bottom', 0);
      }
      else if ($('body').hasClass('logged-in')) {
        $(headerWrapper).css('margin-bottom', 0);
      }
      else {
        $(headerWrapper).css('margin-bottom', 81);
      }
  }
  function uwmScrollTop(){
    var stop = $('html').offset().top;
    var delay = 0;
    $('body,html').animate({scrollTop: stop}, delay);
    return false;
  }
  function setWindowHeight() {
    $('.uwm-main-content-wrapper').css('min-height', $(window).innerHeight());
    $('.uwm-web-page-body').css('min-height', $(window).innerHeight() - footerHeight * 2);
    $('.uwm-right').css('min-height', $(window).innerHeight() - footerHeight * 2);
    $('.uwm-pub-cat').css('min-height', $(window).innerHeight() - footerHeight * 2);
  }
  function uwmParallaxOn(){
    $($parallaxId).removeClass('P-ON');
    $('body').removeClass('no-parallax-bg');
    $(".is-parallax").each(function () {
      var $bgobj = $(this); // Assigning the object.
      var yPos = 15;
      var coords = 'background-position: center ' + yPos + 'px !important; transition: none;';
      $($parallaxId).addClass('P-ON');
      $bgobj.attr('style', coords);

      $(window).scroll(function () {
        var yPos = -(($window.scrollTop() - $bgobj.offset().top) / 5);
        // Put together our final background position.
        var coords = 'background-position: center ' + yPos + 'px !important; transition: none;';
        // Move the background.
        if ($($parallaxId).hasClass('P-ON')) {
          $bgobj.attr('style', coords);
        }

      });

    });
  }
  function uwmParallaxOff(){
    $($parallaxId).removeClass('P-ON');
    $('body').addClass('no-parallax-bg');
    $($parallaxId).css({'background-position' : '','transition' :''});
  }
  function checkWidth() {
    var windowSize = $(window).width();
    var menuOpen = $('.uwm-header-bar');
    var logoHeight = $('#uwm-logo-header');
    var num = logoHeight.outerHeight(true);
    if (document.body.scrollTop < num || document.documentElement.scrollTop < num) {
      flipHeaderClass();
    }
    if (windowSize > 1012) {

      $(logoHeight).removeClass('hide-logo');

      if (menuOpen.hasClass('bt-menu-open')) {
        $(headerWrapper).css('margin-bottom', 0);
        $('.uwm-header-bar-wrapper').removeClass('fixed-to-top');
      }
      else {
        uwmParallaxOn();
        $(headerWrapper).css('margin-bottom', 0);
        $('.uwm-header-bar-wrapper').addClass('fixed-to-top');
      }

    }
    else {
      $(logoHeight).removeClass('hide-logo');
      $(headerWrapper).css('margin-bottom', 172);
      uwmParallaxOff();

    }
  }
/*===================================
  =        WINDOW LOAD        =
===================================*/
  $(window).on({

    load:function () {
      var headerWrapper = $('.uwm-header-wrapper');
      var logoHeight = $('#uwm-logo-header');
      uwmParallaxOff();
      setWindowHeight();
      checkWidth();

      window.onscroll = function () {
        var menuOpen = $('.uwm-header-bar');
        if (menuOpen.hasClass('bt-menu-open')) {
          $(headerWrapper).css('margin-bottom', 0);
        }
        if ($('body').hasClass('logged-in')) {
          $(headerWrapper).css('margin-bottom', 0);
        }
        else {
          flipHeaderClass();
        }

      };

      var openMenu = function () {
        uwmParallaxOff();
        $('.uwm-top-nav-wrapper').attr({
          'data-nav-visible': 'true'
        });
        $('[href="#navigation"]').attr({
          'aria-expanded': 'true'
        });
        $('.uwm-header-bar').removeClass('bt-menu-close');
        $('.uwm-header-bar').addClass('bt-menu-open');
        $('body').addClass('menu-open');
        $('.uwm-left').addClass('menu-open');
        $(headerWrapper).css('margin-bottom', 0);
        $(logoHeight).removeClass('hide-logo');
        uwmScrollTop();
      };

      // Basic close menu function.
      var closeMenu = function () {
        var windowSize = $(window).width();
        $($parallaxId).removeClass('P-ON');
        $(headerWrapper).css('margin-bottom', 0);
        if (windowSize > 1010) {
          uwmParallaxOn();

        }
        $('.uwm-top-nav-wrapper').attr({
          'data-nav-visible': 'false'
        });
        $('[href="#navigation"]').attr({
          'aria-expanded': 'false'
        });
        $('.uwm-header-bar').removeClass('bt-menu-open');
        $('.uwm-header-bar').addClass('bt-menu-close');
        $('body').removeClass('menu-open');
        $('.uwm-left').removeClass('menu-open');
        checkWidth();
      };

      // Run clicks on the navigation button.
      $('[href="#navigation"]').on('click', function (e) {
        e.preventDefault();
        if ($(this).attr('aria-expanded') === 'true') {
          closeMenu();
        }
        else {
          openMenu();
        }
      });

      // At end of navigation block, return focus to navigation menu button.
      $('#navigation li:last-child a').on('keydown', function (e) {
        if (e.keyCode === 9) {
          if (!e.shiftKey) {
            e.preventDefault();
            $('[href="#navigation"]').focus();
          }
        }
      });

      // At start of navigation block, refocus close button on SHIFT+TAB.
      $('#navigation li:first-child a').on('keydown', function (e) {
        if (e.keyCode === 9) {
          if (e.shiftKey) {
            e.preventDefault();
            $('[href="#navigation"]').focus();
          }
        }
      });

      $('#navigation li a').on('click',function () {
        closeMenu();
        uwmScrollTop();
      });
      // If the menu is visible, always TAB into it from the menu button.
      $('[aria-expanded]').on('keydown', function (e) {
        if (e.keyCode === 9) {
          if ($(this).attr('aria-expanded') == 'true') {
            if (!e.shiftKey) {
              e.preventDefault();
              $('#navigation li:first-child a').focus();
            }
            else if (e.shiftKey) {
              e.preventDefault();
              $('.uwm-main-content').focus();
            }
          }
        }
      });
    },
    resize:function () {
      checkWidth();
    }

  });

  $(document).ready(handleDocumentReady);
  /*===================================
  =      DOM READY LOAD     =
  ===================================*/
  function handleDocumentReady() {
    setWindowHeight();
    // checkWidth();
  }
})(window.jQuery, window, window.document);
