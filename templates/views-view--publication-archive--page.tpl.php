<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 *
 * @ingroup views_templates
 */
?>
<div class="uwm-archives-page">
    <div class="uw-section--inner">
        <div class="uwm-pub-cat">
            <ul class="">
              <?php foreach($contents as $content): ?>
                  <li id="uwm-pub-cat-listing-81" class="uwm-pub-cat-listing uwm-animate ">
                      <a class="uwm-archive-alias" href="<?php print $content['tid']; ?>">
                          <div class="uwm-pub-archive-img-wrap ">
                              <div class="uwm-pub-archive-img">
                                  <?php print $content['field_cover_image']; ?>
                              </div>
                          </div>
                          <div class="uwm-pub-archive-info">
                              <h3 class="uwm-pub-cat-headline"><?php print $content['name_field']; ?></h3>
                              <p class="uwm-pub-cat-exerpt"><?php print $content['description_field']; ?> </p>
                          </div>
                      </a>
                  </li>
              <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
