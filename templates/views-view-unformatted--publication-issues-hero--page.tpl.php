<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<?php $hero = $view->result[0]; ?>

<style>
  .uwm-hero-feature {
    background-image: url("<?php print $hero->featured_image_rectangle; ?>");
  }
  @media only screen and (min-width: 48.063em){
    .uwm-hero-feature {
      background-image: url("<?php print $hero->featured_image_original; ?>");
    }
  }
</style>

<div id="uwm-hero-feature" class="uwm-hero-feature">
  <div class="uw-section--inner">
    <a href="<?php print url($hero->_field_data['nid']['entity']->path['source']) ?>">
      <div class="uwm-img-tran">
        <div class="uwm-hero-info">

          <h2 class="uwm-hero-headline">
            <?php print render($hero->field_title_field[0]['rendered']); ?>
          </h2>

          <p class="uwm-hero-sub-headline">
            <?php if(isset($hero->field_field_publication_sub_headline[0])): ?>
              <?php print render($hero->field_field_publication_sub_headline[0]['rendered']); ?>
            <?php endif; ?>
          </p>

          <div class="hero-button">
            <span class="button">Read More</span>
          </div>

        </div>
      </div>
    </a>
  </div>
</div>

<div class="uwm-on-this-page">
  <div class="uw-section--inner">
    <div class="uwm-issue-title " id="uwm-issue-title">

      <h2>
        <span class="uwm-capitalize">
          Inside <span> <?php print render($hero->field_field_issue[0]['rendered']); ?></span>
        </span>
      </h2>

    </div>
  </div>
</div>
