<?php

/**
 * @file
 * Template file for HTML.
 */
?>
<!DOCTYPE html><?php global $base_path; ?>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"
  <?php print $rdf_namespaces; ?>>

<head>
  <?php print $head; ?>
  <link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon" />
  <title><?php print $head_title; ?></title>
  <meta name="msapplication-navbutton-color" content="#000000" />
  <meta name="msapplication-TileColor" content="#000000"/>
  <meta name="msapplication-square70x70logo" content="/university-of-waterloo-logo-tile-70.png"/>
  <meta name="msapplication-square150x150logo" content="/university-of-waterloo-logo-tile-150.png"/>
  <meta name="msapplication-wide310x150logo" content="/university-of-waterloo-logo-tile-310x150.png"/>
  <meta name="msapplication-square310x310logo" content="/university-of-waterloo-logo-tile-310.png"/>
  <link rel="apple-touch-icon" href="/university-of-waterloo-logo-57.png" sizes="57x57">
  <link rel="apple-touch-icon" href="/university-of-waterloo-logo-72.png" sizes="72x72">
  <link rel="apple-touch-icon" href="/university-of-waterloo-logo-76.png" sizes="76x76">
  <link rel="apple-touch-icon" href="/university-of-waterloo-logo-114.png" sizes="114x114">
  <link rel="apple-touch-icon" href="/university-of-waterloo-logo-120.png" sizes="120x120">
  <link rel="apple-touch-icon" href="/university-of-waterloo-logo-144.png" sizes="144x144">
  <link rel="apple-touch-icon" href="/university-of-waterloo-logo-152.png" sizes="152x152">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Barlow+Condensed:wght@500;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@300;500&display=swap" rel="stylesheet">
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <!--[if lte IE 7]><script src="<?php print $base_path ?><?php print drupal_get_path('theme', 'uw_core_theme') ?>/scripts/iconfont.lte-ie7.js"></script><![endif]-->
  <!--[if IE 7]><link rel="stylesheet" type="text/css" href="<?php print $base_path ?><?php print drupal_get_path('theme', 'uw_core_theme') ?>/css/ie7.css"><![endif]-->
  <?php
  // Create empty array to hold any necessary Google Analytics codes.
  $codes = [];
  // Determine if Google Tag Manager is enabled.
  $tag_manager = module_exists('google_tag');
  // If Google Tag Manager is off, we add the global code if it's enabled.
  if (!$tag_manager && variable_get('google_analytics_enable') == 1) {
    $codes['ua'][] = 'UA-51776731-1';
  }
  // If a client-specified code is entered, we add that code.
  if (variable_get('uw_cfg_google_analytics_account')) {
    $code = check_plain(variable_get('uw_cfg_google_analytics_account'));
    $first_letter = substr($code, 0, 1);
    if ($first_letter == 'U') {
      $codes['ua'][] = $code;
    }
    elseif ($first_letter == 'G') {
      $codes['g'][] = $code;
    }
    // Ignoring codes that don't start with the right letter, so no "else" here.
  }
  // If there are any UA codes, we add legacy Google Analytics.
  if (!empty($codes['ua'])) {
    ?>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      <?php
      foreach ($codes['ua'] as $id => $code) {
        echo "ga('create', '" . $code . "', 'auto', {'name': 'tracker" . $id . "'});\n";
        echo "ga('tracker" . $id . ".send', 'pageview');\n";
      }
      ?>
    </script>
    <?php
  }
  // If there are any G codes, we add Google Analytics 4.
  if (!empty($codes['g'])) {
    foreach ($codes['g'] as $code) {
      ?>
      <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $code; ?>"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '<?php echo $code; ?>', {
          'cookie_domain': '<?php echo $_SERVER['HTTP_HOST']; ?>',
       	  'cookie_path': '<?php echo base_path(); ?>',
       	  'cookie_expires': 2592000
        });
      </script>
    <?php
    }
  }
  ?>
  <?php
    if(variable_get('publication_theme', 'publication') == 'magazine') {
        $pub_class = "magazine";
    }
    else{
        $pub_class = "publication";
    }?>
<body class=" <?php print $pub_class; ?> <?php print $classes;?>" <?php print $attributes;?>>
<!--[if lt IE 7]><div id="ie6message">Your version of Internet Explorer web browser is insecure, not supported by Microsoft, and does not work with this web site. Please use one of these links to upgrade to a modern web browser: <a href="http://www.mozilla.org/firefox/">Firefox</a>, <a href="http://www.google.com/chrome">Google Chrome</a>, <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home">Internet Explorer</a>.</div><![endif]-->
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>


</body>
</html>
