<?php

/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>

<div class="uwm-pub-article-page">
    <div class="uw-section--inner">
        <div  id="main" class="uw-site-main--pub" role="main">
            <div id="content" class="uw-site-content">
                <style>
                  .uwm-article-cover-parallax {
                    background-image: url('<?php print $content['featured_image_url']['rectangle'] ?>');
                  }
                  @media only screen and (min-width: 48.063em){
                    .uwm-article-cover-parallax {
                      background-image: url('<?php print $content['featured_image_url']['original'] ?>');
                    }
                  }
                </style>
                <div class="uwm-article-cover-parallax full-width is-parallax">

                </div>

                <!-- check to see if publication type is magazine-->
                <?php if(variable_get('publication_theme', 'publication') == 'magazine'): ?>
                <div class="uwm-article-wrap-wide magazine-article">
                     <div class="uw-section--inner meta">
                         <div class="uwm-article-meta">
                             <span><?php print render($node->field_issue['und'][0]['taxonomy_term']->name); ?> </span>
                         </div>
                     </div>
                     <div id="uwm-article" class="uwm-article">
                         <div class="uw-section--inner">
                            <h1 class="uwm-article-headline"><?php print $node->title; ?></h1>
                            <h2 class="uwm-article-sub-headline">
                              <?php print render($content['field_publication_sub_headline'][0]['#markup']) ?>
                            </h2>
                            <p class="uwm-article-author">
                              <?php if (isset($content['author']['link'])): ?>
                                <a href="<?php print $content['author']['link']; ?>">
                              <?php endif; ?>
                              <?php print $content['author']['name']; ?>
                              <?php if (isset($content['author']['link'])): ?>
                                </a>
                              <?php endif; ?>
                            </p>
                             <?php if(isset($content['sub_navs'])): ?>
                             <!--sub article nav-->
                             <div class="uwm-sub-articles-nav">
                                     <div class="uwm-sub-articles">
                                         <ul class="uwm-sub-list">
                                             <?php foreach ($content['sub_navs'] as $key => $sub_nav) : ?>
                                                 <li class="uwm-sub-item">
                                                     <style>
                                                         .uwm-sub-icon-<?php print $key; ?> {
                                                         <?php if (isset($sub_nav['image']) && $sub_nav['image'] !== NULL && isset($sub_nav['image']['url'])): ?>
                                                             background-image: url("<?php print $sub_nav['image']['url']; ?>");
                                                         <?php endif; ?>
                                                         }
                                                     </style>
                                                     <a href="<?php print base_path() . $sub_nav['path']; ?>">
                                                         <div class="uwm-sub-icon uwm-sub-icon-<?php print $key; ?>"></div>
                                                         <span class="uwm-sub-text"><?php print $sub_nav['title']; ?></span>
                                                     </a>
                                                 </li>
                                             <?php endforeach; ?>
                                         </ul>
                                     </div>
                                 </div>
                             <?php endif; ?>
                             <div class="uwm-social-share">
                                <div class="uwm-social-share-on"> Share:</div>
                                <div class="uwm-social-share-btns">
                                    <?php print render($content['sharing']); ?>
                                </div>
                             </div>
                             <!-- content prints here-->
                             <div class="uw-site-content wide">
                                <div class="uwm-article-body">
                                    <?php print render($content['field_publication_main_content']); ?>
                                </div>
                             </div>
                             <div class="uwm-social-share">
                                <div class="uwm-social-share-on"> Share:</div>
                                <div class="uwm-social-share-btns">
                                <?php print render($content['sharing']); ?>
                                </div>
                             </div>
                             <?php if(isset($content['sub_navs'])): ?>
                             <!--sub article nav-->
                             <div class="uwm-sub-articles-nav-bottom">
                                <div class="uwm-sub-articles">
                                   <ul class="uwm-sub-list">
                                       <?php foreach ($content['sub_navs'] as $key => $sub_nav) : ?>
                                           <li class="uwm-sub-item">
                                               <style>
                                                   .uwm-sub-icon-<?php print $key; ?> {
                                                   <?php if (isset($sub_nav['image']) && $sub_nav['image'] !== NULL && isset($sub_nav['image']['url'])): ?>
                                                       background-image: url("<?php print $sub_nav['image']['url']; ?>");
                                                   <?php endif; ?>
                                                   }
                                               </style>
                                               <a href="<?php print base_path() . $sub_nav['path']; ?>">
                                                   <div class="uwm-sub-icon uwm-sub-icon-<?php print $key; ?>"></div>
                                                   <span class="uwm-sub-text"><?php print $sub_nav['title']; ?></span>
                                               </a>
                                           </li>
                                       <?php endforeach; ?>
                                   </ul>
                                </div>
                             </div>
                             <?php endif; ?>
                         </div>
                     </div>
                 </div>

                <!-- check to see if publication type is publication-->
                <?php elseif(variable_get('publication_theme', 'publication') == 'publication'): ?>
                <div class="uwm-article-wrap publication-article">
                    <div class="uwm-transition-bar">
                        <div id="uwm-article" class="uwm-article">
                            <div class="uwm-article-header">
                                <div class="uwm-article-meta">
                                    <span><?php print render($node->field_issue['und'][0]['taxonomy_term']->name); ?> </span>
                                </div>
                                <h1 class="uwm-article-headline"><?php print $node->title; ?></h1>
                                <h2 class="uwm-article-sub-headline">
                                    <?php print render($content['field_publication_sub_headline'][0]['#markup']) ?>
                                </h2>
                                <p class="uwm-article-author">
                                    <?php if (isset($content['author']['link'])): ?>
                                    <a href="<?php print $content['author']['link']; ?>">
                                    <?php endif; ?>
                                        <?php print $content['author']['name']; ?>
                                        <?php if (isset($content['author']['link'])): ?>
                                    </a>
                                        <?php endif; ?>
                                </p>
                            </div>
                            <?php if(isset($content['sub_navs'])): ?>
                                <?php $num_sub_navs = count($content['sub_navs']) >= 7 ? '7' : count($content['sub_navs'])?>
                                <!--sub article nav-->
                                <div class="uwm-sub-articles-nav">
                                    <div class="uwm-sub-articles">
                                        <ul class="uwm-sub-list large-block-grid-<?php print $num_sub_navs; ?>">
                                            <?php foreach ($content['sub_navs'] as $key => $sub_nav) : ?>
                                            <?php
                                            // Display the first seven sub-navigation images if it is more than seven. ?>
                                            <?php if ($key < 7): ?>
                                                <li class="uwm-sub-item <?php if ($sub_nav['path'] == $node->path['alias']): print 'active';
                                               endif; ?>">
                                                    <style>
                                                        .uwm-sub-icon-<?php print $key; ?> {
                                                        <?php if (isset($sub_nav['image']) && $sub_nav['image'] !== NULL && isset($sub_nav['image']['url'])): ?>
                                                            background-image: url("<?php print $sub_nav['image']['url']; ?>");
                                                        <?php endif; ?>
                                                        }
                                                    </style>
                                                    <a href="<?php print base_path() . $sub_nav['path']; ?>">
                                                        <div class="uwm-sub-icon uwm-sub-icon-<?php print $key; ?>"></div>
                                                        <span class="uwm-sub-text"><?php print $sub_nav['title']; ?></span>
                                                    </a>
                                                </li>
                                            <?php endif; ?>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="uwm-social-share">
                                <div class="uwm-social-share-on"> Share:</div>
                                <div class="uwm-social-share-btns">
                                    <?php print render($content['sharing']); ?>
                                </div>
                            </div>
                            <div class="uw-site-content">
                                <div class="uwm-article-body">
                                    <?php print render($content['field_publication_main_content']); ?>
                                </div>
                                <div class="uwm-social-share">
                                    <div class="uwm-social-share-on"> Share:</div>
                                    <div class="uwm-social-share-btns">
                                        <?php print render($content['sharing']); ?>
                                    </div>
                                </div>
                                <?php if(isset($content['sub_navs'])): ?>
                                    <!--sub article nav-->
                                    <div class="uwm-sub-articles-nav-bottom">
                                        <div class="uwm-sub-articles">
                                            <ul class="uwm-sub-list large-block-grid-<?php print $num_sub_navs; ?>">
                                                <?php foreach ($content['sub_navs'] as $key => $sub_nav) : ?>
                                                <?php
                                                // Display the first seven sub-navigation images if it is more than seven. ?>
                                                <?php if ($key < 7): ?>
                                                    <li class="uwm-sub-item <?php if ($sub_nav['path'] == $node->path['alias']): print 'active';
                                                   endif; ?>">
                                                        <style>
                                                            .uwm-sub-icon-<?php print $key; ?> {
                                                            <?php if (isset($sub_nav['image']) && $sub_nav['image'] !== NULL && isset($sub_nav['image']['url'])): ?>
                                                                background-image: url("<?php print $sub_nav['image']['url']; ?>");
                                                            <?php endif; ?>
                                                            }
                                                        </style>
                                                        <a href="<?php print base_path() . $sub_nav['path']; ?>">
                                                            <div class="uwm-sub-icon uwm-sub-icon-<?php print $key; ?>"></div>
                                                            <span class="uwm-sub-text"><?php print $sub_nav['title']; ?></span>
                                                        </a>
                                                    </li>
                                                <?php endif; ?>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <!--end of transition bar-->
                </div>
                <!--end of article wrap-->
                <?php endif; ?>

                <!--Print of Promo item category Testimonies-->
                <?php if (isset($promo_items)): ?>
                    <?php foreach($promo_items as $pi): ?>
                    <style>
                        .uwm-testimonials-<?php print $pi['promo_id']; ?>  {
                        background-image: url('<?php print $pi['promo_bg_rectangle'] ?>');
                    }
                    @media only screen and (min-width: 48.063em){
                        .uwm-testimonials-<?php print $pi['promo_id']; ?> {
                        background-image: url('<?php print $pi['promo_bg_original'] ?>');
                        }
                    }
                    </style>
                <?php
                // If the promo item has the uploaded background image, no extra class has been added. ?>
                <?php if ($pi['promo_bg_filename'] != NULL): ?>
                    <div class="uwm-testimonials-wrap">
                <?php
                // If the promo item has no uploaded background image, the extra class has been added. ?>
                <?php else: ?>
                    <div class="uwm-testimonials-wrap-nobg">
                <?php endif; ?>
                        <div class="uw-section--inner">
                            <div class="uwm-testimonials uwm-testimonials-<?php print $pi['promo_id']; ?>">
                                <div class="uwm-testimonials-content">
                                    <?php if ($pi['promo_display_title'] == 'yes') : ?>
                                    <h2><?php print $pi['promo_title']; ?></h2>
                                    <?php endif; ?>
                                    <div class="uwm-testimonial-body">
                                    <?php print $pi['promo_content_body']; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if(isset($content['prev_article']) || isset($content['next_article'])): ?>
                <div class="uwm-prev-next-articles-wrap">
                    <div class="uwm-related-articles-header">
                        <h2>More about <span><?php print render($node->field_issue['und'][0]['taxonomy_term']->name); ?></span></h2>
                    </div>
                    <div class="uw-section--inner">
                        <ul class="uwm-prev-next">
                            <?php if(isset($content['prev_article'])): ?>
                            <li class="uwm-prev-next-item uwm-previous-article">
                                <a href="<?php print $content['prev_article']['link']; ?>">
                                    <div class="uwm-prev-next-img">
                                        <figure class="effect-lily">
                                            <img src="<?php print $content['prev_article']['image']; ?>" alt="<?php print $content['prev_article']['image_alt']; ?>"/>
                                            <figcaption>
                                                <div class="umw-prev-next-info">
                                                    <p>
                                                        <span class="button">Read More</span>
                                                    </p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="uwm-prev-next-link ">
                                        <div class="uwm-previous-link text-right">&lt; previous</div>
                                        <span class="uwm-prev-next-cat"><?php print $prev_article_cat; ?></span>
                                        <span class="uwm-prev-next-headline"><?php print $content['prev_article']['title']; ?></span>
                                    </div>
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php if(isset($content['next_article'])): ?>
                            <li class="uwm-prev-next-item uwm-next-article">
                                <a href="<?php print $content['next_article']['link']; ?>">
                                    <div class="uwm-prev-next-img">
                                        <figure class="effect-lily">
                                            <img src="<?php print $content['next_article']['image']; ?>" alt="<?php print $content['next_article']['image_alt']; ?>"/>
                                            <figcaption>
                                                <div class="umw-prev-next-info">
                                                    <p>
                                                        <span class="button">Read More</span>
                                                    </p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="uwm-prev-next-link">
                                        <div class="uwm-next-link">next &gt;</div>
                                        <span class="uwm-prev-next-cat"><?php print $next_article_cat; ?></span>
                                        <span class="uwm-prev-next-headline"><?php print $content['next_article']['title']; ?></span>
                                    </div>
                                </a>
                            </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
                <?php endif; ?>
                <?php if(isset($related_article)): ?>

                <div class="uwm-related-articles-wrap">
                    <div class="uwm-related-articles-header">
                        <h2>Related Articles</h2>
                    </div>
                    <div class="uw-section--inner">

                        <div class="uwm-related-articles">
                            <ul class="uwm-related-list">

                                <?php foreach($related_article as $ra): ?>

                                    <li class="uwm-related-item">

                                        <a href="<?php print $ra['link']; ?>">

                                            <div class="uwm-related-img">
                                                <figure class="effect-lily">
                                                    <img alt="<?php print $ra['image_alt']; ?>" src="<?php print $ra['image']; ?>">
                                                    <figcaption>
                                                        <div class="umw-image-info">
                                                            <p>
                                                                <span class="button">Read More</span>
                                                            </p>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>

                                            <div class="uwm-related-info">
                                                <h3 class="uwm-related-headline"><?php print $ra['title']; ?></h3>
                                                <p class="uwm-related-sub"><?php print $ra['sub_headline']; ?></p>
                                            </div>

                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div><!--/section inner-->
</div>
<?php if (isset($nested_article)): ?>
    <div class="uwm-nested-articles">
        <?php foreach ($nested_article as $na): ?>
            <div id="nestedArticle-<?php print $na['nested_id']; ?>" data-nested-id="" class="reveal-nested" data-reveal="" aria-hidden="true" role="dialog">
                <a class="close-reveal-modal close-nestedArticle button"  href="#"  aria-label="Close">Close</a>
                <?php if (!empty($na['image'])): ?>
                <div class="uw-nested-feature"> <img alt="<?php print $na['image_alt']; ?>" src="<?php print $na['image']; ?>"/></div>
                <?php endif; ?>
                <div class="uwm-nested-article-meta">Article Sidebar</span></div>
                <h2 class="uwm-nested-article-headline"><?php print $na['title']; ?></h2>
                <h3 class="uwm-nested-article-sub-headline"><?php print $na['sub_headline']; ?></h3>

                <div class="uwm-nested-article-body-wrap">
                    <div class="uwm-nested-article-body">
                        <div class="clearfix"><?php print $na['nested_article_body']; ?></div>
                    </div>
                </div>
                <a class="close-reveal-modal close-nestedArticle button"  href="#"  aria-label="Close">Close</a>
            </div>
        <?php endforeach; ?>
    </div>

<?php endif; ?>
