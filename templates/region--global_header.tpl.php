<?php

/**
 * @file
 * Default theme implementation to display a region.
 *
 * Available variables:
 * - $content: The content for this region, typically blocks.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can
 *   be one or more of the following:
 *   - region: The current template type, i.e., "theming hook".
 *   - region-[name]: The name of the region
 *     with underscores replaced with
 *     dashes. For example, the page_top
 *     region would have a region-page-top class.
 * - $region: The name of the region variable
 *    as defined in the theme's .info file.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 *
 * @see template_preprocess()
 * @see template_preprocess_region()
 * @see template_preprocess_region()
 * @see template_process()
 */

$front_page = variable_get('site_frontpage');
$site_name = variable_get('site_name');
$uw_theme_branding = variable_get('uw_theme_branding', 'full');
?>
<header id="top" class="uwm-header-wrapper">
    <div class="uw-section--inner">
        <div id ="uwm-header-bar-wrapper"  class="uwm-header-bar-wrapper">
            <div class="uwm-header-bar">
                <div id ="uwm-logo-header" class="uwm-logo-header">
                    <a href="https://uwaterloo.ca/"><span class="show-for-sr">University of Waterloo</span></a>
                </div>
                <div class="fade uwm-site-title">
                    <a href="<?php print url('', array('absolute' => TRUE)) ?>" title="<?php print $site_name; ?>" rel="home">
                        <?php print $site_name; ?>
                        <?php if (isset($site_slogan) && $site_slogan !== "") : ?>
                            <span class="uw-site—subtitle"> <?php print $site_slogan; ?> </span>
                        <?php endif; ?>
                    </a>
                </div>
                <div class="navigation-button-wrap">
                    <a class="navigation-button" aria-label="Navigation" aria-expanded="false" href="#navigation">Menu<span></span></a>
                </div>
            </div>
            <div class="uwm-top-nav-wrapper" data-nav-visible="false">
                <nav id="navigation" class="uwm-top-nav uwm-main-nav" aria-label="Main">
                    <?php
                    $pub_menu = menu_navigation_links('main-menu');
                    print theme('links__main-menu', array('links' => $pub_menu));
                    ?>
                </nav>
            </div>
            <?php if ($uw_theme_branding === 'full') { ?>
                <div id="site-colors" class="uw-site--colors">
                    <div class="uw-section--inner">
                        <div class="uw-site--cbar">
                            <div class="uw-site--c1 uw-cbar"></div>
                            <div class="uw-site--c2 uw-cbar"></div>
                            <div class="uw-site--c3 uw-cbar"></div>
                            <div class="uw-site--c4 uw-cbar"></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
</header>
