<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<?php if(variable_get('publication_theme', 'publication') == 'magazine') { ?>
      <div>
          <h2 class="uwm-also-header">Also in this issue</h2>
      </div>
        <div class="uwm-secondary-block">
            <div class="uwm-secondary-wrap">
                <ul class="uwm-secondary-list">
            <?php foreach ($view->result as $result): ?>
              <li id="<?php print "uwm-secondary-hp-" . $result->nid ?>" class="uwm-secondary-hp uwm-animate">
                <div class="uwm-secondary-meta">
                    <?php print render($result->field_field_publication_category[0]['rendered']) ?>
                </div>
                <a href="<?php print url($result->_field_data['nid']['entity']->path['source'])?>">
                  <div class="uwm-secondary-img">
                    <figure class="effect-lily">
                        <img src="<?php print $result->nonfeatured_image_rectangle; ?>" alt="<?php print $result->nonfeatured_image_rectangle_alt;?>" />
                      <figcaption>
                        <div class="uw-image-info">
                            <p>
                                <span class="button">Read more</span>
                            </p>
                        </div>
                      </figcaption>
                    </figure>
                  </div>
                  <div class="uwm-secondary-info">
                    <h3 class="uwm-secondary-headline">
                      <?php print render($result->field_title_field[0]['rendered'])?>
                    </h3>
                    <span class="uwm-secondary-sub">
                    <?php if(isset($result->field_field_publication_sub_headline[0])): ?>
                        <?php print render($result->field_field_publication_sub_headline[0]['rendered'])?>
                    <?php endif;?>
                    </span>
                  </div>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>

<?php } ?>
    <!-- closes the front wrapper -->
    </div>
</div>
