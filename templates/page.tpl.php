<?php

/**
 * @file
 * Template for almost the entire page. Most of the page is inside div#site.
 */
$uw_theme_branding = variable_get('uw_theme_branding', 'full');

?>
<div class="breakpoint" id="breakpoint"></div>

<div id="site" class="<?php print $classes; ?> uw-site"<?php print $attributes; ?>>
  <div class="uw-site--inner">
    <?php print render($page['global_header']); ?>
    <div class="uw-section--inner">
     <nav id="skip" class="skip" aria-label="Skip links">
        <a href="#main" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to main'); ?></a>
        <a href="#footer" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
      </nav>
    </div>


    <?php if($is_front || $is_archive):?>
    <div class="uw-site-publication">
      <div class="uw-section--inner">
        <div  id="main" class="uw-site-main--pub" role="main">
            <div class="uw-site--messages">
                <?php print $messages; ?>
            </div>

            <div class="uw-site--help">
                <?php print render($page['help']); ?>
            </div>
          <div id="content" class="uw-site-content">
            <?php print render($page['content']); ?>
          </div>
        </div>
      </div><!--/section inner-->
    </div>
    <?php else:?>
    <?php if(isset($content_layout)):?>
        <?php if($content_layout):?>
        <div  id="main" class="uwm-main-content-wrapper">
              <div role="document" id="uwm-main-content" class="uwm-main-content uwm-animate">
                <div class="uwm-on-this-page">
                    <div>
                        <div class="uwm-issue-title">
                            <h1 class="uwm-capitalize"> <?php print $page_title;?></h1>
                        </div>
                    </div>
                </div>
                <div class="uw-section--inner">
                    <div class="uw-site--main-top">

                        <div class="uw-site--banner">
                            <?php print render($page['banner']); ?>
                        </div>

                        <div class="uw-site--messages">
                            <?php print $messages; ?>
                        </div>

                        <div class="uw-site--help">
                            <?php print render($page['help']); ?>
                        </div>

                        <!-- when logged in -->
                        <?php if ($tabs) : ?>
                            <div class="node-tabs uw-site-admin--tabs">
                                <?php print render($tabs); ?>
                            </div>
                        <?php endif; ?>

                        <?php if ($action_links): ?>
                            <ul class="action-links">
                                <?php print render($action_links); ?>
                            </ul>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="uw-section--inner">
                    <div id="content" class="uw-site-content">
                        <?php print render($page['content']); ?>
                    </div>
                </div>
            </div><!--/section inner-->
        </div>
        <?php endif; ?>
    <?php endif; ?>

        <?php if(isset($article_layout)):?>
            <?php if($article_layout):?>
            <div class="uw-section--inner">
                <div class="uw-site--main-top">

                    <div class="uw-site--banner">
                        <?php print render($page['banner']); ?>
                    </div>

                    <div class="uw-site--messages">
                        <?php print $messages; ?>
                    </div>

                    <div class="uw-site--help">
                        <?php print render($page['help']); ?>
                    </div>

                    <!-- when logged in -->
                    <?php if ($tabs) : ?>
                        <div class="node-tabs uw-site-admin--tabs">
                            <?php print render($tabs); ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($action_links): ?>
                        <ul class="action-links">
                            <?php print render($action_links); ?>
                        </ul>
                    <?php endif; ?>

                </div>
            </div>
            <?php print render($page['content']); ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>


  <div id="footer" class="uw-footer" role="contentinfo">
    <?php include drupal_get_path('theme', 'uw_fdsu_theme_resp') . '/templates/site-footer.tpl.php'; ?>
    <div class="uw-section--inner">
      <?php print render($page['global_footer']); ?>
     </div>
  </div>
</div><!--/site-->
<div class="ie-resize-fix"></div>
