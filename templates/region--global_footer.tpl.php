<?php

/**
 * @file
 * Default theme implementation to display a region.
 *
 * Available variables:
 * - $content: The content for this region, typically blocks.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can
 *   be one or more of the following:
 *   - region: The current template type, i.e., "theming hook".
 *   - region-[name]: The name of the region
 *     with underscores replaced with
 *     dashes. For example, the page_top
 *     region would have a region-page-top class.
 * - $region: The name of the region variable
 *    as defined in the theme's .info file.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 */
global  $base_url;
$uw_theme_branding = variable_get('uw_theme_branding', 'full');
$uw_core_theme = drupal_get_path('theme', 'uw_core_theme') . '/templates/';

$site_logo = variable_get('uw_nav_site_footer_logo');
$site_logo_link = variable_get('uw_nav_site_footer_logo_link');

$facebook = variable_get('publication_social_facebook');
$twitter = variable_get('publication_social_twitter');
$instagram = variable_get('publication_social_instagram');
$youtube = variable_get('publication_social_youtube');
$linkedin = variable_get('publication_social_linkedin');
$vimeo = variable_get('publication_social_vimeo');
$alt_tag = str_replace('_', ' ', $site_logo) . ' logo';
$arr = explode(" ", $alt_tag);
$arr = array_unique($arr);
$alt_tag = implode(" ", $arr);

$site_faculty_or_dept_logo = variable_get('publication_faculty_or_dept_logo');
$site_faculty_dept_name = variable_get('publication_faculty_or_dept');
$site_faculty_dept_link = variable_get('publication_faculty_or_dept_link');
$site_subscription_link = variable_get('publication_subscription_link');
$site_support_link = variable_get('publication_support_link');
?>
<div class="uwm-footer-wrapper">
  <div class="uw-section--inner">
    <!-- If pub type is publication and report, then show -->
    <footer id="uwm-footer">
      <div class="uwm-footer">
        <div class="uwm-footer-inner">
          <div class="uwm-top-link-wrapper">
            <a class="uwm-top-link" id=scrollme type="button" href="#breakpoint"><abbr title="Back to top">^</abbr></a>
          </div>
          <div class="uwm-logo-wrapper uw-logo">
          <?php if ($site_faculty_dept_name):?>
            <div class="uwm-affiliate-title ">
              <?php if ($site_faculty_dept_link):?>
                <a class="uwm-footer-link" href="<?php print($site_faculty_dept_link);?>">
              <?php endif; ?>
              <?php print($site_faculty_dept_name);?>
              <?php if ($site_faculty_dept_link):?>
                </a>
              <?php endif; ?>
            </div>
          <?php endif; ?>
          <div class="uwm-affiliate-logo">
            <a class="uwm-footer-logo-link" href="https://uwaterloo.ca/">
              <img src="" alt="" />
            </a>
          </div>
          <div class="uwm-university-logo">
            <a class="uwm-footer-uw-link" href="https://uwaterloo.ca/">
              <img src="<?php print $base_url . '/' . drupal_get_path('theme', 'uw_theme_publication'); ?>/images/uw-logo-white-h.png" alt="University of Waterloo Logo" />
            </a>
          </div>
        </div>
        <nav class="uwm-footer-nav">
        <?php
          $pub_footer_menu = menu_navigation_links('menu-footer-menu');
          print theme('links__menu-footer-menu', array('links' => $pub_footer_menu));
        ?>
        </nav>
        <div class="uwm-action-wrapper">
        <?php
          if (((variable_get('publication_theme', 'publication') == 'publication') && isset($site_subscription_link)  && $site_subscription_link !== '')
            || ((variable_get('publication_theme', 'publication') == 'magazine') && isset($site_support_link)  && $site_support_link !== '')): ?>
          <?php if (variable_get('publication_theme', 'publication') == 'magazine'): ?>
            <div class="uwm-support-button">
              <a class="button" href="<?php print($site_support_link);?>">Support Waterloo</a>
            </div>
          <?php else: ?>
            <div class="uwm-subscribe-button">
              <a class="button" href="<?php print($site_subscription_link);?>">Subscribe</a>
            </div>
          <?php endif; ?>
        <?php else: ?>
          <div class="uwm-social-tag">
            <a href="https://twitter.com/search?q=%23UWATERLOOINNOVATION">#uwaterlooinnovation</a>
          </div>
          <div class="uwm-social-links">
            <ul>
              <?php if ((string) $facebook !== ''): ?>
                <li>
                  <a href="https://www.facebook.com/<?php print check_plain($facebook); ?>" aria-label="facebook">
                    <span class="ifdsu fdsu-facebook"></span><span class="show-for-sr">facebook</span>
                  </a>
                </li>
              <?php endif; ?>
              <?php if ((string) $twitter !== ''): ?>
                <li>
                  <a href="https://www.twitter.com/<?php check_plain(print $twitter); ?>" aria-label="twitter">
                    <span class="ifdsu fdsu-twitter"></span><span class="show-for-sr" >twitter</span>
                  </a>
                </li>
              <?php endif; ?>
              <?php if ((string) $instagram !== ''): ?>
                <li>
                  <a href="https://www.instagram.com/<?php print check_plain($instagram); ?>" aria-label="instagram">
                    <span class="ifdsu fdsu-instagram"></span><span class="show-for-sr" >instagram</span>
                  </a>
                </li>
              <?php endif; ?>
              <?php if ((string) $linkedin !== ''): ?>
                <li>
                  <a href="https://www.linkedin.com/<?php check_plain(print $linkedin); ?>" aria-label="linkedin">
                    <span class="ifdsu fdsu-linkedin"></span><span class="show-for-sr" >linkedin</span>
                  </a>
                </li>
              <?php endif; ?>
              <?php if ((string) $vimeo !== ''): ?>
                <li>
                  <a href="https://www.vimeo.com/<?php check_plain(print $vimeo); ?>" aria-label="vimeo">
                    <span class="ifdsu fdsu-vimeo"></span><span class="show-for-sr" >vimeo</span>
                  </a>
                </li>
              <?php endif; ?>
              <?php if ((string) $youtube !== ''): ?>
                <li>
                  <a href="https://www.youtube.com/<?php check_plain(print $youtube); ?>" aria-label="youtube">
                    <span class="ifdsu fdsu-youtube"></span><span class="show-for-sr" >youtube</span>
                  </a>
                </li>
              <?php endif; ?>
            </ul>
          </div>
        <?php endif; ?>
        </div>
        <?php require ($uw_core_theme .'global_territorial.tpl.php');?>
      </div>

      <!-- full width mid gray bar at bottom -->
      <div class="uwm-social">
        <div class="uwm-social-inner">
        <?php
          if (((variable_get('publication_theme', 'publication') == 'publication') && isset($site_subscription_link)  && $site_subscription_link !== '')
            || ((variable_get('publication_theme', 'publication') == 'magazine') && isset($site_support_link)  && $site_support_link !== '')): ?>
            <div class="uwm-social-bar">
              <div class="uwm-social-tag">
                <a href="https://twitter.com/search?q=%23UWATERLOOINNOVATION">#uwaterlooinnovation</a>
              </div>
              <div class="uwm-social-links">
                <ul>
                  <?php if ((string) $facebook !== ''): ?>
                    <li>
                      <a href="https://www.facebook.com/<?php print check_plain($facebook); ?>" aria-label="facebook">
                        <span class="ifdsu fdsu-facebook"></span><span class="show-for-sr">facebook</span>
                      </a>
                    </li>
                  <?php endif; ?>
                  <?php if ((string) $twitter !== ''): ?>
                    <li>
                      <a href="https://www.twitter.com/<?php check_plain(print $twitter); ?>" aria-label="twitter">
                        <span class="ifdsu fdsu-twitter"></span><span class="show-for-sr" >twitter</span>
                      </a>
                    </li>
                  <?php endif; ?>
                  <?php if ((string) $instagram !== ''): ?>
                    <li>
                      <a href="https://www.instagram.com/<?php print check_plain($instagram); ?>" aria-label="instagram">
                        <span class="ifdsu fdsu-instagram"></span><span class="show-for-sr" >instagram</span>
                      </a>
                    </li>
                  <?php endif; ?>
                  <?php if ((string) $linkedin !== ''): ?>
                    <li>
                      <a href="https://www.linkedin.com/<?php check_plain(print $linkedin); ?>" aria-label="linkedin">
                        <span class="ifdsu fdsu-linkedin"></span><span class="show-for-sr" >linkedin</span>
                      </a>
                    </li>
                  <?php endif; ?>
                    <?php if ((string) $vimeo !== ''): ?>
                      <li>
                        <a href="https://www.vimeo.com/<?php check_plain(print $vimeo); ?>" aria-label="vimeo">
                          <span class="ifdsu fdsu-vimeo"></span><span class="show-for-sr" >vimeo</span>
                        </a>
                      </li>
                    <?php endif; ?>
                    <?php if ((string) $youtube !== ''): ?>
                      <li>
                        <a href="https://www.youtube.com/<?php check_plain(print $youtube); ?>" aria-label="youtube">
                          <span class="ifdsu fdsu-youtube"></span><span class="show-for-sr" >youtube</span>
                        </a>
                      </li>
                    <?php endif; ?>
                  </ul>
                </div>
              </div>
          <?php endif ?>
          <div class="uwm-login">
            <?php print $cas_login; ?>
          </div>
        </div>
      </div>
    </footer>
  </div>
</div>
