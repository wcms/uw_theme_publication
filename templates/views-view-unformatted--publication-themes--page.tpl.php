<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="uwm-publication-category-wrap">
    <div class="uwm-pub-cat">
        <ul>
            <?php foreach($view->result as $result): ?>

                <?php $issue_path = url($result->_field_data['nid']['entity']->path['source'])?>
                <li id="<?php print('uwm-pub-cat-listing-' . $result->nid) ?>" class="uwm-pub-cat-listing uwm-animate">
                    <a  class="uwm-pub-cat-img-link" href="<?php print($issue_path)?>">
                        <div class="uwm-pub-cat-img-wrap">
                            <figure class="effect-lily">
                                <img src="<?php print $result->theme_image_rectangle; ?>" alt="<?php print $result->theme_image_rectangle_alt; ?>"/>
                                <figcaption>
                                    <div class="umw-image-info">
                                        <p>
                                            <span class="button">Read More</span>
                                        </p>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                    </a>
                    <div class="uwm-pub-cat-info">
                        <div class="uwm-pub-cat-issue">
                            <?php print render($result->field_field_issue[0]['rendered'])?>
                        </div>
                        <h3 class="uwm-pub-cat-headline">
                            <a href="<?php print($issue_path)?>">
                                <?php print render($result->field_title_field[0]['rendered'])?>
                            </a>
                        </h3>
                        <p class="uwm-pub-cat-subheadline">
                            <?php if(isset($result->field_field_publication_sub_headline[0])): ?>
                                <?php print render($result->field_field_publication_sub_headline[0]['rendered'])?>
                            <?php endif;?>
                        </p>
                        <p class="uwm-pub-cat-exerpt">
                            <?php print render($result->field_field_publication_teaser[0]['rendered'])?>
                        </p>
                        <hr>
                        <span class="uwm-pub-cat-name">
                            <span class="cat-type">Category:</span>
                            <?php foreach($result->field_field_publication_category as $category): ?>
                                <span><?php print render($category['rendered'])?></span>
                            <?php endforeach ?>
                            </span>
                        </span>
                        <div class="uwm-pub-cat-theme">
                            <span class="cat-theme">Theme:</span>
                            <?php foreach($result->field_field_taxonomy_theme as $theme): ?>
                                <span><?php print render($theme['rendered'])?></span>
                            <?php endforeach ?>
                        </div>
                    </div>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
</div>
