<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<div class="uwm-front-wrapper">
  <div class="uw-section--inner">

    <!-- If publication type is "publication" -->
    <?php if (variable_get('publication_theme', 'publication') == 'publication') { ?>

      <div class="uwm-pub-cat">
        <ul>

          <?php foreach ($view->result as $result): ?>

            <li id="<?php print "uwm-pub-cat-listing-" . $result->nid ?>" class="uwm-pub-cat-listing fade">

              <a class="uwm-pub-cat-img-link" href="<?php print url($result->_field_data['nid']['entity']->path['source'])?>">
                <div class="uwm-pub-cat-img-wrap">
                  <div class="uwm-pub-cat-img">
                    <figure class="effect-lily">
                      <img src="<?php print $result->featured_image_rectangle; ?>"  alt="<?php print $result->featured_image_rectangle_alt;?>"/>
                      <figcaption>
                        <div class="umw-image-info">
                          <p><span class="button">Read more</span></p>
                        </div>
                      </figcaption>
                    </figure>
                  </div>
                </div>
              </a>

              <div class="uwm-pub-cat-info">
                <div class="uwm-pub-cat-issue"></div>

                <h3 class="uwm-pub-cat-headline">
                  <a href="<?php print url($result->_field_data['nid']['entity']->path['source'])?>"><?php print render($result->field_title_field[0]['rendered'])?></a>
                </h3>

                <?php if(isset($result->field_field_publication_sub_headline[0])): ?>
                  <p class="uwm-pub-cat-subheadline"><?php print render($result->field_field_publication_sub_headline[0]['rendered'])?></p>
                <?php endif;?>

                <p class="uwm-pub-cat-exerpt">
                  <?php print render($result->field_field_publication_teaser[0]['rendered'])?>
                </p>

                <hr>

                <span class="uwm-pub-cat-name">
                  <span class="cat-type">Category:</span>
                  <span>

                  <?php foreach($result->field_field_publication_category as $category): ?>
                    <?php print render($category['rendered'])?>
                  <?php endforeach ?>

                  </span>
                </span>

              </div>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    <?php } ?>

    <!-- If publication type is "magazine" -->
    <?php if (variable_get('publication_theme', 'publication') == 'magazine') { ?>

      <div class="uwm-feature-hp-block">
        <div class="uwm-feature-hp-wrap">

          <ul class="uwm-feature-hp-list">
            <?php foreach ($view->result as $result): ?>

              <li id="<?php print "uwm-feature-hp-" . $result->nid ?>" class="uwm-feature-hp">
                <a href="<?php print url($result->_field_data['nid']['entity']->path['source'])?>">
                  <div class="uwm-feature-hp-img">
                    <figure class="effect-lily">
                      <img src="<?php print $result->featured_image_cover; ?>" alt="<?php print $result->featured_image_cover_alt;?>">
                      <figcaption>
                        <div class="umw-image-info">
                          <p><span class="button">Read More</span></p>
                        </div>
                      </figcaption>
                    </figure>
                  </div>

                  <div class="uwm-feature-hp-info">

                    <h3 class="uwm-feature-hp-headline">
                      <?php print render($result->field_title_field[0]['rendered'])?>
                    </h3>

                    <p class="uwm-feature-hp-sub">
                      <?php if(isset($result->field_field_publication_sub_headline[0])): ?>
                        <?php print render($result->field_field_publication_sub_headline[0]['rendered'])?>
                      <?php endif;?>
                    </p>

                    <p class="uwm-feature-hp-teaser">
                      <?php print render($result->field_field_publication_teaser[0]['rendered'])?>
                    </p>
                  </div>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>

        </div>
      </div>
    <?php } ?>

    <?php if (variable_get('publication_theme', 'publication') == 'publication') : ?>

      <div class="more-feature">
        <?php print l(t('More featured articles'), 'categories/feature', array('attributes' => array('class' => array('button')))); ?>
      </div>

    <?php endif; ?>
