<?php

/**
 * @file
 * The uWaterloo Publication theme and preprocess functions.
 */

/**
 * Implements hook_preprocess_region().
 *
 * Add cas login link to footer.
 */
function uw_theme_publication_preprocess_region(&$vars) {

  // If we are on the footer region, add the cas login link as a variable.
  if ($vars['region'] == 'global_footer') {

    // Load in the cas loging link block.
    $block = array(block_load('uw_auth_cas_common', 'cas'));
    $block = _block_render_blocks($block);
    $block = _block_get_renderable_array($block);

    // Create a variable for the cas login link.
    $vars['cas_login'] = render($block);
  }
}

/**
 * Implements hook_preprocess_views_view().
 */
function uw_theme_publication_preprocess_views_view(&$vars) {

  // If we are on the publication archives view, process it.
  if (isset($vars['view']) && $vars['view']->name == 'publication_archive') {

    // Array to store the rows.
    $contents = array();

    // Render all fields.
    $rows_rendered = $vars['view']->style_plugin->render_fields($vars['view']->result);

    // Step through each rendered row and add to variable.
    foreach ((array) $rows_rendered as $key => $row) {

      // Step through each row in that is rendered,
      // and add to variable.
      foreach ($row as $name => $val) {

        // If it is the term id, get the alias,
        // otherwise get the value.
        if ($name == 'tid') {
          $contents[$key][$name] = drupal_get_path_alias('taxonomy/term/' . $val);
        }
        else {
          $contents[$key][$name] = $val;
        }
      }
    }

    // Put the variable into the vars array to be used in the template.
    $vars['contents'] = $contents;

  }

}

/**
 * Get the previous or next info on a publication article node.
 */
function _uw_theme_publication_get_article_info($nid) {

  // Load the node.
  $node = node_load($nid);

  // Get the entity.
  $entity = entity_metadata_wrapper('node', $node);

  // Set the title from the node.
  $article['title'] = $node->title;

  // Set the image using the image style.
  $article['image'] = image_style_url(
    'size540x370',
    $entity->field_feature_image->value()['uri']
  );

  // Get the alt of the image.
  $article['image_alt'] = $entity->field_feature_image->value()['alt'];

  // Get the sub headline.
  $article['sub_headline'] = $entity->field_publication_sub_headline->value();

  // Get all the categories.
  $categories = $entity->field_publication_category->value();

  // If there are categories, step through and set them.
  if (count($categories) > 0) {

    // Step through each category and set the variable.
    foreach ($categories as $category) {
      $article['categories'][] = $category->name;
    }
  }

  $themes = $entity->field_taxonomy_theme->value();

  // If there are Themes, step through and set them.
  if (count($themes) > 0) {

    // Step through each category and set the variable.
    foreach ($themes as $theme) {
      $article['themes'][] = $theme->name;
    }
  }

  // Set the link to the article.
  $article['link'] = url(drupal_get_path_alias('node/' . $node->nid));

  // Return the article.
  return $article;

}

/**
 * Get the previous or next info on a publication article node.
 */
function _uw_theme_publication_get_nested_article_info($nid) {

  // Load the node.
  $node = node_load($nid);

  // Get the entity.
  $entity = entity_metadata_wrapper('node', $node);

  // Set the title from the node.
  $nestedArticle['title'] = $node->title;

  $nestedArticle['nested_id'] = $nid;

  // Set the image using the image style.
  if (!empty($entity->field_feature_image->value()['uri'])) {
    $nestedArticle['image'] = image_style_url(
      'size540x370',
      $entity->field_feature_image->value()['uri']
    );

    // Get the alt of the image.
    $nestedArticle['image_alt'] = $entity->field_feature_image->value()['alt'];
  }
  else {
    $nestedArticle['image'] = '';
    $nestedArticle['image_alt'] = '';
  }

  // Get the sub headline.
  $nestedArticle['sub_headline'] = $entity->field_publication_sub_headline->value();

  // Get the content.
  $nestedArticle['nested_article_body'] = $node->body['en'][0]['safe_value'];

  // Return the article.
  return $nestedArticle;

}

/**
 * Helper function.
 *
 * Get the published promo items info.
 */
function _uw_theme_publication_get_promo_info($nids, &$promo_items) {

  // Step through each node.
  foreach ($nids as $nid) {
    // Load the node.
    $node = node_load($nid);

    // Only process the node which current status is published.
    // '1' indicates workbench moderation current published.
    // '0' indicates workbench moderation current unpublished.
    if (isset($node->workbench_moderation['current']->published) && $node->workbench_moderation['current']->published == 1) {
      $entity = entity_metadata_wrapper('node', node_load($nid));

      // Get the node id.
      $promo_item['promo_id'] = $nid;

      // Get the title.
      $promo_item['promo_title'] = $entity->title->value();

      // Check to see if Display title is active.
      $promo_item['promo_display_title'] = 'yes';

      if ($node->field_do_not_display_title['und'][0]['value'] == 1) {
        $promo_item['promo_display_title'] = 'no';
      }

      // Get the body.
      $promo_item['promo_content_body'] = $entity->field_body_no_summary->value()['safe_value'];

      // Get the original background image url.
      $promo_item['promo_bg_original'] = image_style_url(
        'original',
        $entity->field_background_image->value()['uri']
      );

      // Get the rectangle background image url.
      $promo_item['promo_bg_rectangle'] = image_style_url(
        'size540x370',
        $entity->field_background_image->value()['uri']
      );

      // Get the original background image filename.
      $promo_item['promo_bg_filename'] = $entity->field_background_image->value()['filename'];

      // Add to the promo items array.
      $promo_items[] = $promo_item;
    }

  }
}

/**
 * Implements template_preprocess_node().
 *
 * Add social media sharing block to publication article node.
 */
function uw_theme_publication_preprocess_node(&$variables) {

  // If a publication article, add social media sharing block.
  if ($variables['node']->type == 'publication_article') {

    // Query to get the promotional items that are testimonials.
    $query = db_select('node', 'n');
    $query->join('field_data_field_promo_categories', 'fpc', 'fpc.entity_id = n.nid');
    $query->fields('n', array('nid'));
    $query->condition('n.type', 'uw_promotional_item', '=');
    $query->condition('fpc.field_promo_categories_value', 'Testimonies', '=');

    // The results from the query.
    $results = $query->execute();

    // Step through each result and get the nid.
    while ($record = $results->fetch()) {

      // Set the nid.
      $nids[] = $record->nid;
    }

    // If there are promo items (i.e. nids), process them.
    if (isset($nids) && count($nids) > 0) {

      // Array to be used for the promo items.
      $promo_items = array();

      // Function to get the promo item info.
      _uw_theme_publication_get_promo_info($nids, $promo_items);

      // If there are promo items set the variable.
      if (count($promo_items) > 0) {

        // Set the promo items variable.
        $variables['promo_items'] = $promo_items;

      }
    }
    if (isset($variables['content']['field_previous_article'])) {

      // Get the node id of the previous article.
      $prev_nid = $variables['content']['field_previous_article'][0]['#item']['target_id'];

      // Get the variables required for the previous article.
      $variables['content']['prev_article'] = _uw_theme_publication_get_article_info($prev_nid);

      // Create a variable 'prev_article_cat' to store previous article category.
      // If only has one category, it will display the only one.
      if (count($variables['content']['prev_article']['categories']) == 1) {
        $variables['prev_article_cat'] = $variables['content']['prev_article']['categories'][0];
      }
      // If more than one category, it will display the first one order by weight asc except 'Feature'.
      else {
        if (!in_array("Feature", $variables['content']['prev_article']['categories'])) {
          $variables['prev_article_cat'] = $variables['content']['prev_article']['categories'][0];
        }
        else {
          // Feature is always [0] if it is selected, we skip it.
          $variables['prev_article_cat'] = $variables['content']['prev_article']['categories'][1];
        }
      }
    }

    if (isset($variables['content']['field_next_article'])) {

      // Get the node id of the next article.
      $next_nid = $variables['content']['field_next_article'][0]['#item']['target_id'];

      // Get the variables required for the next article.
      $variables['content']['next_article'] = _uw_theme_publication_get_article_info($next_nid);

      // Create a variable 'next_article_cat' to store next article category.
      // If only has one category, it will display the only one.
      if (count($variables['content']['next_article']['categories']) == 1) {
        $variables['next_article_cat'] = $variables['content']['next_article']['categories'][0];
      }
      // If more than one category, it will display the first one order by weight asc except 'Feature'.
      else {
        if (!in_array("Feature", $variables['content']['next_article']['categories'])) {
          $variables['next_article_cat'] = $variables['content']['next_article']['categories'][0];
        }
        else {
          // Feature is always [0] if it is selected, we skip it.
          $variables['next_article_cat'] = $variables['content']['next_article']['categories'][1];
        }
      }
    }
    // Every item in related articles assigned to display.
    if (isset($variables['content']['field_nested_articles'])) {
      // Loop through them and call to set variables .
      foreach ($variables['content']['field_nested_articles']['#items'] as $nestedArticle) {

        // Creating array to store required values for display.
        $variables['nested_article'][] = _uw_theme_publication_get_nested_article_info($nestedArticle['node']->nid);

      }

    }

    // Unset the fields as not required anymore.
    unset($variables['content']['field_previous_article']);
    unset($variables['content']['field_next_article']);
    unset($variables['content']['field_nested_articles']);

    // Original image style for the featured image.
    $variables['content']['featured_image_url']['original'] = image_style_url(
      'original',
      $variables['content']['field_feature_image']['#items'][0]['uri']
    );

    // Rectangle image style for the featured image.
    $variables['content']['featured_image_url']['rectangle'] = image_style_url(
      'size540x370',
      $variables['content']['field_feature_image']['#items'][0]['uri']
    );

    // Load in social media sharing block.
    $block = module_invoke('uw_social_media_sharing', 'block_view', 'social_media_block');

    // Unset the google+ and email sharing.
    unset($block['content']['#items'][2]);
    unset($block['content']['#items'][4]);

    // Set block to variables array.
    $variables['content']['sharing'] = $block['content'];

    // If there are related articles process them.
    if (isset($variables['content']['field_related_articles'])) {

      // Every item in related articles assigned to display.
      foreach ($variables['content']['field_related_articles']['#items'] as $article) {

        // Creating array to store required values for display.
        $variables['related_article'][] = _uw_theme_publication_get_article_info($article['node']->nid);
      }
    }

    // Get the required author information and unset render array.
    $entity = entity_metadata_wrapper('node', $variables['node']);
    $variables['content']['author']['name'] = $entity->field_author_collection[0]->field_author_name->value();
    $variables['content']['author']['link'] = $entity->field_author_collection[0]->field_profile_link->value()['url'];
    unset($variables['content']['field_author_collection']);

    // Get the sub navigation from the node.
    $sub_navs = $entity->field_subnavigation_articles->value();

    // If there are sub navs process them.
    if (count($sub_navs) > 0) {

      // Step through each sub nav and get info.
      foreach ($sub_navs as $sub_nav) {

        // Set the image variable to null so it resets on the loop.
        $image = NULL;

        // Get the entity for the sub nav.
        $entity_sub_nav = entity_metadata_wrapper('node', $sub_nav->nid);

        // Set the icon image.
        // If Sub-navigation icon image field is not empty, we use its value as icon image.
        if ($entity_sub_nav->field_uw_pub_sub_nav_icon_image->value()) {
          $icon_image = $entity_sub_nav->field_uw_pub_sub_nav_icon_image->value();

          // If there is an icon image, process it.
          if (isset($icon_image) && count($icon_image) > 0) {

            // Set the image URL and ALT.
            $image['url'] = file_create_url($icon_image['uri']);
            $image['alt'] = $icon_image['alt'];
          }
        }
        // If If Sub-navigation icon image field is empty, we use the value of field_image as icon image.
        else {
          $icon_image = $entity_sub_nav->field_image->value();

          // If there is an icon image, process it.
          if (isset($icon_image[0]) && count($icon_image) > 0) {

            // Set the image URL and ALT.
            // The number of values of field_image is unlimited, so we select the first one.
            $image['url'] = file_create_url($icon_image[0]['uri']);
            // field_image has no alt, so we set the publication article title as image alt.
            $image['alt'] = $entity_sub_nav->title->value();
          }
        }

        // Set the variables to be used on the template.
        $variables['content']['sub_navs'][] = array(
          'title' => $entity_sub_nav->title->value(),
          'path' => $sub_nav->path['alias'],
          'image' => isset($image) ? $image : '',
        );
      }
    }
  }
}

/**
 * Add theme suggestion for all content types.
 */
function uw_theme_publication_preprocess_page(&$variables) {

  // The URL arguments.
  $args = arg();

  // Check if we are on a term page, and if so add proper classes.
  if (in_array('taxonomy', $args) && in_array('term', $args) && is_numeric(end($args))) {

    // Load the taxonomy term.
    $term = taxonomy_term_load(end($args));

    // Ensure that we are on a publication issue, then set variables.
    if ($term->vocabulary_machine_name == 'publication_issue') {

      $variables['is_archive'] = TRUE;

    }

    else {

      $variables['content_layout'] = TRUE;
      $variables['is_archive'] = FALSE;
      $variables['page_title'] = $term->name_field['en'][0]['safe_value'];
    }

  }
  else {

    // Set variables when we are on uw_web_page and term pages.
    $menu_object = menu_get_object();
    $term = menu_get_object('taxonomy_term', 2);
    $variables['is_archive'] = FALSE;

    // If not on the front page, check for term and not term,
    // set appropriate layout variables.
    if (!$variables['is_front']) {

      // If term page, set content layout.
      if ($term) {

        // Set appropriate variables.
        $variables['content_layout'] = TRUE;
        $variables['page_title'] = $term->name;

      }
      // If on publication article, set article layout.
      elseif (isset($menu_object->type) && $menu_object->type == 'publication_article') {

        // Set appropriate variables.
        $variables['article_layout'] = TRUE;

      }
      // Not term or article page, set content layout.
      else {

        // Set the content layout.
        $variables['content_layout'] = TRUE;

        // Set the page title based on menu object or title from Drupal.
        if (isset($menu_object->title)) {

          $variables['page_title'] = $menu_object->title;
        }
        else {

          $variables['page_title'] = drupal_get_title();
        }
      }
    }
  }

}

/**
 * Implements hook_preprocess_html().
 *
 * Add title on homepage.
 */
function uw_theme_publication_preprocess_html(&$variables) {

  // If on the front page.
  if ($variables['is_front']) {

    // Get the arguments from the URL.
    $args = arg();

    // If on a vocabulary page with a term, add the title.
    if (in_array('vocabulary', $args) && in_array('issues', $args) && is_numeric(end($args))) {

      // Get the term using the tid from the arguments.
      $term = taxonomy_term_load(end($args));

      // Set the head title using the term name.
      $variables['head_title'] = $term->name . ' ' . $variables['head_title'];

    }

  }
}
